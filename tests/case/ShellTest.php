<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Shell;

use Nora;

/**
 * Shellのテスト
 *
 */
class ShellTest extends \PHPUnit_Framework_TestCase
{
    public function testShell ( )
    {
        Nora::logging_start()->addLogger([
            'writer' => 'stdout'
        ]);


        $cmd = Nora::module('shell')->command('cat');

        $cmd->options()->set('-n', '-');

        if ($cmd->open())
        {
            $cmd->inputFile(__file__);
            if($cmd->status());
            var_Dump(stream_get_contents($cmd->stderr()));
            var_Dump(stream_get_contents($cmd->stdout()));
        }
    }
}
