<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */

// オートローダを登録する
Nora::Autoloader( )->addLibrary(
    realpath(__DIR__.'/..').'/class',
    'Nora\Module\Shell'
);


