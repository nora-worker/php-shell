<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Shell;

use Nora\Core\Module;

/**
 * Shellモジュール
 */
class Facade implements Module\ModuleIF
{
    use Module\Modulable;

    protected function initModuleImpl( )
    {
        $this->setHelper( [
            'command' => ['scope', function ($scope, $name, $options = []) {

                $cmd = new Command( );
                $cmd->setScope($scope);
                $cmd->setName($name);
                $cmd->setOptions($options);

                return $cmd;
            }]
        ]);
    }
}
