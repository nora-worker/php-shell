<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Shell;

use Nora\Core\Component\Component;

/**
 * Shellコマンドオプション
 */
class Options
{
    private $_vars = [];

    public function set($opt)
    {
        if (is_array($opt))
        {
            foreach ($opt as $v) $this->set($v);
            return $this;
        }

        if (func_num_args() > 1)
        {
            foreach(func_get_args() as $v) {
                $this->set($v);
            }
            return $this;
        }

        $this->_vars[] = $opt;
        return $this;
    }

    public function setIf($q)
    {
        if (!$q) {
            return $this;
        }
        return call_user_func_array([$this,'set'], array_slice(func_get_args(),1));
    }


    private function build( )
    {
        $opts = '';
        foreach ($this->_vars as $v)
        {
            $opts.= ' '.escapeshellarg($v);
        }
        return  $opts;
    }

    public function __toString ( )
    {
        return $this->build();
    }
}
