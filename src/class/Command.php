<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Shell;

use Nora\Core\Component\Component;

/**
 * Shellコマンド
 */
class Command extends Component
{
    private $_name;
    private $_options;
    private $_pipes;
    private $_proc = null;
    private $_stdout;
    private $_stderr;
    private $_ret;

    protected function bootOptions( )
    {
        return new Options( );
    }

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    public function setOptions($options = [])
    {
        $this->options()->set($options);
        return $this;
    }

    public function isOpend( )
    {
        return $this->_proc === null ? false: true;
    }

    public function open ( )
    {
        $this->_proc = proc_open(
            $this->build(),
            [
                0 => ['pipe', 'r'],
                1 => ['pipe', 'w'],
                2 => ['pipe', 'w']
            ],
            $this->_pipes
        );

        if (!is_resource($this->_proc)) {
            $this->alert('cmd '.$this->build(). ' can not be opened');
            return false;
        }

        $this->logDebug([
            'cmd' => $this->build()
        ]);
        return true;
    }

    public function inputFile ($file, $close = true)
    {
        $fp = fopen($file, 'r');
        flock($fp, LOCK_SH);
        while(!feof($fp))
        {
            $this->input(fread($fp, 1024), false);
        }
        flock($fp, LOCK_UN);
        fclose($fp);

        return $this->input(null, $close);
    }

    public function input ($var, $close = true)
    {
        if (!$this->isOpend()) $this->open();

        fwrite($this->_pipes[0], $var);

        if ($close === true) {
            return $this->close();
        }
        return true;
    }

    public function close( )
    {
        // 入力を閉じる
        fclose($this->_pipes[0]);
        stream_copy_to_stream($this->_pipes[1], $this->_stdout = tmpfile());
        stream_copy_to_stream($this->_pipes[2], $this->_stderr = tmpfile());
        fclose($this->_pipes[1]);
        fclose($this->_pipes[2]);

        // プロセスを閉じる
        $this->_ret = proc_close($this->_proc);

        if ($this->_ret > 0)
        {
            $this->logWarning(stream_get_contents($this->stderr()));
        }

        $this->_proc = null;
        return $this->_ret;
    }

    public function stdout()
    {
        rewind($this->_stdout);
        return $this->_stdout;
    }

    public function stderr()
    {
        rewind($this->_stderr);
        return $this->_stderr;
    }

    public function getOutput( )
    {
        return stream_get_contents($this->stdout());
    }

    public function getError( )
    {
        return stream_get_contents($this->stderr());
    }

    public function status( )
    {
        return $this->_ret;
    }


    private function build ( )
    {
        return escapeshellcmd($this->_name).$this->options();
    }

    public function __toString( )
    {
        return $this->build();
    }

}

